package com.programadoresbr.blogservice.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.programadoresbr.blogservice.model.Post;

public interface PostRepository extends ReactiveMongoRepository<Post, String>{

}
