package com.programadoresbr.blogservice.model;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import reactor.util.annotation.Nullable;

@Document
public class Post {

	@Id
	private String id;
	
	@TextIndexed
	@NotNull
	private String body;
	
	@NotNull
	private String title;
	
	@NotNull
	private Long userId;
	
	@Nullable
	List<Coment> coments;
	
	@Nullable
	List<String> images;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Coment> getComents() {
		return coments;
	}

	public void setComents(List<Coment> coments) {
		this.coments = coments;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}
	
	
	
	
	
}
