package com.programadoresbr.blogservice.model;

import java.util.UUID;

public class Coment {

	UUID id = UUID.randomUUID();
	
	String coment;
	
	Long userId;

	public String getComent() {
		return coment;
	}

	public void setComent(String coment) {
		this.coment = coment;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	
	
}

