package com.programadoresbr.blogservice.service;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.springframework.http.codec.multipart.FilePart;

import com.programadoresbr.blogservice.model.Coment;
import com.programadoresbr.blogservice.model.Post;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PostService {

	Mono<Post> save(Flux<FilePart> images, Post post, Long userId) throws InterruptedException, ExecutionException;
	
	Flux<Post> find(String search, int pageSize, int page);
	
	Mono<Post> findById(String id);
	
	Mono<Post> update(Post post);
	
	Mono<Post> doComent(String PostId, Coment coment, Long userId);
	
	Mono<Void> likeOneComent(String PostId, UUID ComentId);
	
	Mono<Void> dislikeOneComent(String PostId, UUID ComentId);
	
	
	
}
