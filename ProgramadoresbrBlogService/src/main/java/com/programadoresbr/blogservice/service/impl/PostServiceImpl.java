package com.programadoresbr.blogservice.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.MissingRequiredPropertiesException;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.http.codec.multipart.FilePart;

import com.programadoresbr.blogservice.error.exception.MissingRequiredPropsException;
import com.programadoresbr.blogservice.error.exception.PostNotFoundException;
import com.programadoresbr.blogservice.model.Coment;
import com.programadoresbr.blogservice.model.Post;
import com.programadoresbr.blogservice.repository.PostRepository;
import com.programadoresbr.blogservice.service.PostService;
import com.programadoresbr.blogservice.util.FileUtil;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class PostServiceImpl implements PostService {

	@Autowired
	private PostRepository repository;

	@Autowired
	FileUtil fileUtil;

	@Autowired
	private ReactiveMongoTemplate template;
	
	
	@Override
	public Mono<Post> save(Flux<FilePart> images, Post post, Long userId)
			throws InterruptedException, ExecutionException {
		if (images.collectList().toFuture().get().isEmpty()) {
			post.setUserId(userId);
			return repository.save(post);
		} else {

			List<String> paths = new ArrayList<>();
			post.setUserId(userId);
			return repository.save(post).flatMap(p -> {

				String id = post.getId();
				Mono<List<String>> path = images.filter(part -> part instanceof FilePart).ofType(FilePart.class)
						.flatMap(file -> {
							try {
								return Mono.just(fileUtil.salvarArquivo(file, "/blog_posts/", id));
							} catch (IOException e) {
								Mono.error(e);
								return null;
							}
						}).collectList();
				try {
					path.toFuture().get().iterator().forEachRemaining(imagePath -> paths.add(imagePath));
				} catch (InterruptedException | ExecutionException e) {
					Mono.error(e);
				}
				post.setImages(paths);

				return repository.save(post);
			});

		}
	}

	@Override
	public Flux<Post> find(String search, int pageSize, int page) {
		TextCriteria criteria = TextCriteria.forLanguage("portuguese").caseSensitive(false).matchingPhrase(search).matchingAny(search);
		Query query = TextQuery.queryText(criteria)
				.sortByScore()
				.with(PageRequest.of(page, pageSize));
		
		return template.find(query, Post.class);
		}

	@Override
	public Mono<Post> findById(String id) {
		return repository.findById(id).onErrorResume(e -> Mono.error(new PostNotFoundException("Post " + id + "não encontrado")));
	}

	@Override
	public Mono<Post> update(Post post) {
		if(post.getId().isBlank() || post.getId() == null) {
			Mono.error(new MissingRequiredPropsException("É preciso informar o Id do post para fazer um Update"));
		}
		return repository.save(post);
		
	}

	@Override
	public Mono<Post> doComent(String postId, Coment coment, Long userId) {
		coment.setUserId(userId);
		Mono<Post> fallback = Mono.error(new PostNotFoundException("Post " + postId + "não encontrado"));
		Mono<Post> post = repository.findById(postId).switchIfEmpty(fallback);
	}

	@Override
	public Mono<Void> likeOneComent(String PostId, UUID ComentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Void> dislikeOneComent(String PostId, UUID ComentId) {
		// TODO Auto-generated method stub
		return null;
	}

}
