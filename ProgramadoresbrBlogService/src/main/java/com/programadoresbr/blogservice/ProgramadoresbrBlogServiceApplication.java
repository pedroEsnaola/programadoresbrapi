package com.programadoresbr.blogservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramadoresbrBlogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgramadoresbrBlogServiceApplication.class, args);
	}

}

