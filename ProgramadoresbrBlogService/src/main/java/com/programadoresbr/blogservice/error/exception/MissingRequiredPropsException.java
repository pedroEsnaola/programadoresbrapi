package com.programadoresbr.blogservice.error.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MissingRequiredPropsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MissingRequiredPropsException(String message) {
		super(message);
	}
}
