package com.programadoresbr.blogservice.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.programadoresbr.blogservice.model.Post;
import com.programadoresbr.blogservice.service.impl.PostServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("/blog/post")
public class PostController {

	@Autowired
	private PostServiceImpl postService;
	
	@Autowired
	private ReactiveMongoTemplate template;
	
	ObjectMapper mapper;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Post> save(@RequestPart(required = false) Flux<FilePart> arquivo,
			@RequestPart("post") String json, @RequestParam("user") Long userId) throws InterruptedException, ExecutionException	{
			Post post = new Post();
		
			try {
				post = mapper.readValue(json , Post.class);
			} catch (JsonParseException e) {
				Mono.error(e);
			} catch (JsonMappingException e) {
				Mono.error(e);
			} catch (IOException e) {
				Mono.error(e);
			}
		
		
		
		return postService.save(arquivo, post, userId);
	}
	
	
}
