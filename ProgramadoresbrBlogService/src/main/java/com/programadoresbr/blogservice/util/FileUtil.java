package com.programadoresbr.blogservice.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;

@Component
public class FileUtil {

	private String path = "/programadoresbr";

	public String salvarArquivo(FilePart arquivo, String diretorio, String id) throws IOException {

		Path diretorioPath = Paths.get(this.path, diretorio);
		Path arquivoPath = diretorioPath.resolve(arquivo.filename().replace(arquivo.filename(), id));

		try {
			Files.createDirectories(diretorioPath);
			arquivo.transferTo(arquivoPath.toFile());
			return path + diretorio + "/" + id;
		} catch (IOException e) {
			throw e;
		}
	}

}
