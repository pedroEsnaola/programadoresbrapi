package com.programadoresbr.error.exception;

import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class MissingRequiredPropsException extends RuntimeException {

	public MissingRequiredPropsException(BindingResult result) {
		this.errors = result.getFieldErrors();
	}
	
	List<FieldError> errors;

	public List<FieldError> getErrors() {
		return errors;
	}

	public void setErrors(List<FieldError> errors) {
		this.errors = errors;
	}
	
	
}
