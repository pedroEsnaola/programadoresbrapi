package com.programadoresbr.error.exception;

public class MissingRequiredPropertyException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MissingRequiredPropertyException(String message) {
		super(message);
	}

}
