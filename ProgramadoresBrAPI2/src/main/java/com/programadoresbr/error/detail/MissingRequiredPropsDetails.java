package com.programadoresbr.error.detail;

import java.util.List;

import org.springframework.validation.FieldError;
import javax.annotation.Generated;
import java.util.Collections;

public class MissingRequiredPropsDetails extends ErrorDetails {
	
	String fields;

	@Generated("SparkTools")
	private MissingRequiredPropsDetails(Builder builder) {
		this.title = builder.title;
		this.status = builder.status;
		this.detail = builder.detail;
		this.timestamp = builder.timestamp;
		this.developerMessage = builder.developerMessage;
		this.fields = builder.fields;
	}

	/**
	 * Creates builder to build {@link MissingRequiredPropsDetails}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder newBuilder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link MissingRequiredPropsDetails}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String title;
		private int status;
		private String detail;
		private Long timestamp;
		private String developerMessage;
		private String fields;

		private Builder() {
		}

		public Builder withTitle(String title) {
			this.title = title;
			return this;
		}

		public Builder withStatus(int status) {
			this.status = status;
			return this;
		}

		public Builder withDetail(String detail) {
			this.detail = detail;
			return this;
		}

		public Builder withTimestamp(Long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder withDeveloperMessage(String developerMessage) {
			this.developerMessage = developerMessage;
			return this;
		}

		public Builder withFields(String fields) {
			this.fields = fields;
			return this;
		}

		public MissingRequiredPropsDetails build() {
			return new MissingRequiredPropsDetails(this);
		}
	}

	

	
}
