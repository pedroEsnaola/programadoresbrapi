package com.programadoresbr.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.programadoresbr.model.Post;
import com.programadoresbr.model.PostProjection;

@Repository
public interface PostRepository extends PagingAndSortingRepository<Post, Long> {

	@Query(value = "SELECT * FROM post WHERE title LIKE CONCAT('%', ?1 ,'%') OR body LIKE CONCAT('%', ?1 ,'%') ",nativeQuery = true)
	Iterable<PostProjection> findPostsByTitle(String title);

}
