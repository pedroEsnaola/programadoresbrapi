package com.programadoresbr.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.programadoresbr.model.Feedback;

@Repository
public interface FeedBackRepository extends CrudRepository<Feedback, Long> {

	@Override
	@Query(nativeQuery = true, value = "SELECT * FROM feedback WHERE deleted_at = null")
	Iterable<Feedback> findAll();

}
