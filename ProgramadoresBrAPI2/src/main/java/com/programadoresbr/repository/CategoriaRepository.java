package com.programadoresbr.repository;

import org.springframework.data.repository.CrudRepository;

import com.programadoresbr.model.Categoria;

public interface CategoriaRepository extends CrudRepository<Categoria, Long> {

}
