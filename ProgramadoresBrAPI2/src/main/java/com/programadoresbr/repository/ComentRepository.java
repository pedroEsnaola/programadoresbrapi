package com.programadoresbr.repository;

import org.springframework.data.repository.CrudRepository;

import com.programadoresbr.model.Coment;

public interface ComentRepository extends CrudRepository<Coment, Long> {

}
