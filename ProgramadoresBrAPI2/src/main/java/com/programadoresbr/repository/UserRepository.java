package com.programadoresbr.repository;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.programadoresbr.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	
	
	User findByEmail(String email);

	void deleteByEmail(String email);

	boolean existsByEmail(String email);
	
	Page<User> findAll(Pageable pageable);

}
