package com.programadoresbr.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.programadoresbr.model.PasswordToken;

@Repository
public interface PasswordTokenRepository extends CrudRepository<PasswordToken, Long> {
	
	PasswordToken findByToken(String toke);

}
