package com.programadoresbr;

import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.programadoresbr.util.JWTUtil;

import jdk.jfr.Enabled;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@EnableAsync
@EnableCaching
@EnableSpringDataWebSupport
@EnableDiscoveryClient
@SpringBootApplication(exclude= {UserDetailsServiceAutoConfiguration.class})
public class ProgramadoresBrApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgramadoresBrApiApplication.class, args);
	}

	int maxPollSize = 10;

	@Bean
	public Scheduler jdbcScheduler() {
		return Schedulers.fromExecutor(Executors.newFixedThreadPool(maxPollSize));
	}
	
	@Bean
	public JWTUtil jwtUtil() {
		return new JWTUtil();
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder(4);
	}
	
}
