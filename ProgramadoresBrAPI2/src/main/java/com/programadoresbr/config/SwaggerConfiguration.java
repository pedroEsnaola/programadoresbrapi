package com.programadoresbr.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.web.reactive.config.ResourceHandlerRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurationSupport;

import com.fasterxml.jackson.databind.ObjectMapper;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;

@Configuration
@EnableSwagger2WebFlux
class SwaggerConfiguration extends WebFluxConfigurationSupport {

  @Bean
  public Jackson2JsonEncoder jackson2JsonEncoder(ObjectMapper mapper) {
      return new Jackson2JsonEncoder(mapper);
  }

  @Bean 
  public Jackson2JsonDecoder jackson2JsonDecoder(ObjectMapper mapper) {
      return new Jackson2JsonDecoder(mapper);
  }

  @Override
  protected void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/**").addResourceLocations("classpath:META-INF/resources/");
  }
  
  @Bean
  public Docket api() {
	  return new Docket(DocumentationType.SWAGGER_2)
			  .select()
			  .apis(RequestHandlerSelectors.any())
			  .build()
			  .apiInfo(info());
  }
  
  public ApiInfo info() {
	  @SuppressWarnings("unchecked")
	ApiInfo info = new ApiInfo("ProgramadoresBR API",
			  "API utilizada nos servicos da pagina programadoresbr.com",
			  "0.2","",
			  new Contact("ProgramadoresBR", "https://www.youtube.com/programadoresbr , https://programadoresbr.com.br/"
					  , "contato@programadoresbr.com"), "", "",Collections.EMPTY_LIST);
	  return info;
  }

}
