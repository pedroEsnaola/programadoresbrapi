package com.programadoresbr.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

import com.programadoresbr.security.CustomAuthenticationManager;
import com.programadoresbr.security.SecurityContextRepository;

@Configuration
@EnableWebFluxSecurity
public class SecurityConfig  {

	private CustomAuthenticationManager authManager;

	private SecurityContextRepository securityRepository;
	
	
	@Autowired
	public SecurityConfig(CustomAuthenticationManager authManager, SecurityContextRepository securityRepository) {
		super();
		this.authManager = authManager;
		this.securityRepository = securityRepository;
	}

	@Bean
	public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
		
		http.csrf().disable();
		http.formLogin().disable();
		http.logout().disable();
		http.httpBasic().disable();
		
		return http.authenticationManager(authManager).securityContextRepository(securityRepository).authorizeExchange()
				.pathMatchers("/protected/**").authenticated().pathMatchers("/unprotected/**").permitAll().pathMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security", "/swagger-ui.html", "/webjars/**", "/**").permitAll()
				.anyExchange().authenticated().and().build();

	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
