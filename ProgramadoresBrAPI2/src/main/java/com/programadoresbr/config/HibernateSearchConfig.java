package com.programadoresbr.config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HibernateSearchConfig {

	 private final EntityManager em;

	    @Autowired
	    public HibernateSearchConfig(final EntityManagerFactory entityManagerFactory) {
	        this.em = entityManagerFactory.createEntityManager();
	        
	    }

	
	@Bean
	@Transactional
	FullTextEntityManager fullTextEntityManager() {
		FullTextEntityManager ftem = Search.getFullTextEntityManager(em);
		try {
			ftem.createIndexer().startAndWait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return ftem;
	}
	
}
