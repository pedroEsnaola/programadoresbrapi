package com.programadoresbr.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {

	@Bean
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setHost("mail.programadoresbr.com.br");
		mailSender.setPort(587);
		mailSender.setUsername("contato@programadoresbr.com.br");
		mailSender.setPassword("Ba$72jlmhcPV");

		java.util.Properties mailProperties = new java.util.Properties();
		mailProperties.put("mail.smtp.auth", true);
		mailProperties.put("mail.smtp.starttls.enable", false);
		mailProperties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		mailProperties.put("mail.smtp.ssl.enable", false);
		mailProperties.put("mail.transport.protocol", "smtp");
		mailProperties.put("mail.debug", true);

		mailSender.setJavaMailProperties(mailProperties);
		return mailSender;
	}
}
