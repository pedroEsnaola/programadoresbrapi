package com.programadoresbr.security;

import java.util.List;

import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.programadoresbr.model.TipoCadastro;
import com.programadoresbr.util.JWTUtil;

import io.jsonwebtoken.SignatureException;
import reactor.core.publisher.Mono;

@Component
public class CustomAuthenticationManager implements ReactiveAuthenticationManager {

	private JWTUtil jwtUtil = new JWTUtil();

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		String authToken = authentication.getCredentials().toString();

		String username;
		try {
			username = jwtUtil.getUsernameFromToken(authToken);
		} catch (Exception e) {
			username = null;
		}
		try {
			if (username != null && jwtUtil.validateToken(authToken)) {
				String password = jwtUtil.getPasswordFromToken(authToken);
				List<TipoCadastro> roles = jwtUtil.getRolesFromToken(authToken);
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(username, password,
						roles);
				return Mono.just(auth);
			} else {
				return Mono.empty();
			}
		} catch (SignatureException e) {

			return Mono.empty();
		}

	}
}