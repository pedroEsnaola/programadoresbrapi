package com.programadoresbr.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.programadoresbr.model.Post;
import com.programadoresbr.model.PostProjection;
import com.programadoresbr.service.impl.PostServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/unprotected/post")
@CrossOrigin
public class PostController {

	@Autowired
	PostServiceImpl postService;


	@GetMapping("/title/{title}")
	@ResponseStatus(code = HttpStatus.OK)
	public Flux<PostProjection> findPosts(@PathVariable String search,
			@RequestParam(required = true,name = "page") int page,
			@RequestParam(required = true, name = "pageSize") int pageSize){
		return postService.findPosts(search, page, pageSize);
	}

	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Post> findPost(@PathVariable Long id) {
		return postService.findPostById(id);
	}

	
}
