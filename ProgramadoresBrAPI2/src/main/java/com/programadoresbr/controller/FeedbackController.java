package com.programadoresbr.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.programadoresbr.model.Feedback;
import com.programadoresbr.service.impl.FeedbackServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/feedback")
@CrossOrigin
public class FeedbackController {

	@Autowired
	FeedbackServiceImpl feedbackService;

	@GetMapping
	@PreAuthorize("ADMIN")
	@ResponseStatus(code = HttpStatus.OK)
	Flux<Feedback> findAll() {
		return feedbackService.getAllFeedbacks();
	}

	@PostMapping("/new")
	@ResponseStatus(code = HttpStatus.OK)
	Mono<Feedback> newFeedback(@RequestBody Feedback feedback, Principal principal) {
		return feedbackService.newFeedback(feedback, principal.getName());
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("ADMIN")
	@ResponseStatus(code = HttpStatus.OK)
	Mono<Feedback> deleteFeedback(@RequestParam Long id) {
		return feedbackService.deleteFeedbackById(id);
	}
}
