package com.programadoresbr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.julienvey.trello.domain.Card;
import com.programadoresbr.service.impl.TrelloServiceImpl;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/unprotected/trelloapi")
@CrossOrigin
public class TrelloController {

	private TrelloServiceImpl trelloService;

	@Autowired
	public TrelloController(TrelloServiceImpl trelloService) {
		super();
		this.trelloService = trelloService;
	}

	@GetMapping("/1/boards")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<?> GetBoards() {
		return Mono.just(trelloService.getBoards());
	}

	@GetMapping("/1/boards/{id}/cards")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<?> getCardsByBoardId(@PathVariable String id) {
		return Mono.just(trelloService.getCards(id));
	}

	@PostMapping("/1/cards")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<?> addCard(@RequestBody Card card) {
		return Mono.just(trelloService.addCard(card));
	}

}
