
package com.programadoresbr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebExchangeBindException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.programadoresbr.model.AuthRequest;
import com.programadoresbr.model.Status;
import com.programadoresbr.model.TipoCadastro;
import com.programadoresbr.model.User;
import com.programadoresbr.service.impl.UserServiceImpl;
import com.programadoresbr.util.JWTUtil;

import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
@RequestMapping("/unprotected/user")
public class UserController {

	@Autowired
	private UserServiceImpl uService;

	@Autowired
	private BCryptPasswordEncoder encoder;

	private JWTUtil jwtUtil = new JWTUtil();
	
	ObjectMapper mapper = new ObjectMapper();

	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<User> salvarUsuario(@RequestBody @Valid User user) {
		
		
		if (user.getTipoCadastro() == null) {
			List<TipoCadastro> roles = new ArrayList<>();
			TipoCadastro tipoCadastro = TipoCadastro.USER;
			roles.add(tipoCadastro);
			user.setTipoCadastro(roles);
		}
		return uService.saveUser(user);
		
	}


	@PostMapping("/auth")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Map<String, String>> auth(@RequestBody AuthRequest ar) {
		Mono<User> usuario = uService.findByEmail(ar.getEmail());
		return usuario.map((user) -> {
			// Verifica a senha
			if (encoder.matches(ar.getPassword(), user.getSenha()) && user.getStatus().equals(Status.ATIVADO)) {
				// Faz o Build do Token
				String token = "Bearer " + jwtUtil.generateToken(user);
				Map<String, String> tokenReturn = new HashMap<>();
				tokenReturn.put("Authorization", token);
				return tokenReturn;
			} else {
				// Lanca uma exception caso algo de errado
				throw new BadCredentialsException("Senha incorreta");
			}
		});
	}
	
	@GetMapping("/count")
	public Mono<Long> count(){
		return uService.count();
	}
	
	@PostMapping("/password/change/{email}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Void> requestPasswordChange(@PathVariable String email){
		return uService.requestPasswordChange(email);
	}
	
	@PutMapping("/password/change/{token}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<User> changePassword(@RequestBody AuthRequest newPassword, @PathVariable String token) throws JsonParseException, JsonMappingException, IOException{
		return uService.changePassword(newPassword.getPassword(), token);
	}
	
	@PutMapping("/activate/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<User> activateUser(@PathVariable Long id){
		return uService.activateUser(id);
	}
	
	

}
