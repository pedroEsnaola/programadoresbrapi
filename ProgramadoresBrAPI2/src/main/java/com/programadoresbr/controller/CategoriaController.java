package com.programadoresbr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.programadoresbr.model.Categoria;
import com.programadoresbr.repository.CategoriaRepository;

import io.swagger.annotations.Api;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/categoria")
@Api(value = "End-point utilizado para adicionar e remover categorias do Fórum")
@CrossOrigin
public class CategoriaController {

	@Autowired
	private CategoriaRepository repository;

	@PostMapping("/add")
	@ResponseStatus(code = HttpStatus.OK)
	Mono<Categoria> addCategoria(@RequestBody Categoria categoria) {
		return Mono.just(repository.save(categoria));
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	void deletar(@PathVariable Long id) {
		repository.deleteById(id);
	}

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	Flux<Categoria> findAll() {
		return Flux.fromIterable(repository.findAll());
	}

}
