package com.programadoresbr.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.programadoresbr.model.User;
import com.programadoresbr.service.impl.UserServiceImpl;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/protected/user")
@CrossOrigin
public class ProtectedUserController {

	@Autowired
	private UserServiceImpl uService;
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	Mono<Void> delete(@PathVariable long id) {
		uService.deleteById(id);
		return Mono.empty();
	}

	@PostMapping("/profilepic")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<User> changeProfilePic(@RequestPart FilePart foto, Principal principal) {
		return uService.changeProfilePic(foto, principal.getName());
	}
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	@PreAuthorize("ADMIN")
	public Mono<Page<User>> findUsers(Pageable pageable) {
		return uService.findAll(pageable);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<User> findById(@PathVariable Long id){
		return uService.findById(id);
	}
	
	@GetMapping("/me")
	public Mono<String> me(Principal principal) {
		return Mono.just(principal.getName());
	}
	
}
