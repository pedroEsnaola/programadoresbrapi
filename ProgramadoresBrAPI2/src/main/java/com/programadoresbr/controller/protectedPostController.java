package com.programadoresbr.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.programadoresbr.model.Coment;
import com.programadoresbr.model.Post;
import com.programadoresbr.service.impl.PostServiceImpl;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/protected/post")
@CrossOrigin
public class protectedPostController {
	
	@Autowired
	PostServiceImpl postService;
	
	@PostMapping()
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Post> save(@RequestPart(required = false) Flux<FilePart> arquivo,
			@RequestPart("post") String json, Principal principal) {
		return postService.newPost(arquivo, json, principal.getName());
	}
	@PutMapping("/update")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Post> updatePost(@RequestBody Post post, Principal principal) {
		return postService.updatePost(post, principal.getName());
	}

	@PostMapping("/coment/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Post> doComent(@PathVariable Long id, Principal principal, @RequestBody Coment coment) {
		return postService.doComent(id, coment, principal.getName());
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<Post> deletePost(@PathVariable Long id, Principal principal) {
		return postService.deletePost(id, principal.getName());
	}
	@PostMapping("/coment/like/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<ResponseEntity> likeOneComent(@PathVariable Long id) {
		postService.likeOneComent(id);
		return Mono.just(new ResponseEntity(HttpStatus.OK));
	}

	@PostMapping("/coment/dislike/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Mono<ResponseEntity> dislikeOneComent(@PathVariable Long id) {
		postService.dislikeOneComent(id);
		return Mono.just(new ResponseEntity(HttpStatus.OK));
	}
}
