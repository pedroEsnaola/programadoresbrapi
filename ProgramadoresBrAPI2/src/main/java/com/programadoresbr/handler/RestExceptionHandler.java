package com.programadoresbr.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;

import com.programadoresbr.error.detail.BadCredentialsDetails;
import com.programadoresbr.error.detail.GenericErrorDetails;
import com.programadoresbr.error.detail.MissingRequiredPropsDetails;
import com.programadoresbr.error.detail.UserAlreadyExistsDetails;
import com.programadoresbr.error.exception.MissingRequiredPropertyException;
import com.programadoresbr.error.exception.MissingRequiredPropsException;
import com.programadoresbr.error.exception.NotValidTokenException;
import com.programadoresbr.error.exception.UserAlreadyExistsException;
import com.programadoresbr.error.exception.UserNotFoundException;

@ControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(UserAlreadyExistsException.class)
	public ResponseEntity<?> handleUserAlreadyExistsException(UserAlreadyExistsException uaeException) {
		UserAlreadyExistsDetails uaeDetails = UserAlreadyExistsDetails.newBuilder().withTimestamp(new Date().getTime())
				.withTitle("Email já cadastrado").withStatus(HttpStatus.BAD_REQUEST.value())
				.withDetail(uaeException.getMessage()).withDeveloperMessage(uaeException.getClass().getName()).build();

		return new ResponseEntity<>(uaeDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<?> handleBadCredentialException(BadCredentialsException bcException) {
		BadCredentialsDetails uaeDetails = BadCredentialsDetails.newBuilder().withTimestamp(new Date().getTime())
				.withStatus(HttpStatus.UNAUTHORIZED.value()).withDetail(bcException.getMessage())
				.withDeveloperMessage(bcException.getClass().getName()).build();

		return new ResponseEntity<>(uaeDetails, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<?> handleUserNotFoundException(UserNotFoundException unfException) {
		GenericErrorDetails uaeDetails = GenericErrorDetails.newBuilder().withTimestamp(new Date().getTime())
				.withTitle("Usuário não encontrado").withStatus(HttpStatus.NOT_FOUND.value())
				.withDetail(unfException.getMessage()).withDeveloperMessage(unfException.getClass().getName()).build();

		return new ResponseEntity<>(uaeDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(MissingRequiredPropertyException.class)
	public ResponseEntity<?> handleMissingRequiredPropertiesException(MissingRequiredPropertyException mrpException) {
		GenericErrorDetails mrpDetails = GenericErrorDetails.newBuilder().withTimestamp(new Date().getTime())
				.withTitle(mrpException.getMessage()).withStatus(HttpStatus.UNAUTHORIZED.value())
				.withDetail("O id não foi informado").withDeveloperMessage(mrpException.getClass().getName()).build();

		return new ResponseEntity<>(mrpDetails, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(NotValidTokenException.class)
	public ResponseEntity<?> handleNotValidTokenExceptio(NotValidTokenException mrpException) {
		GenericErrorDetails mrpDetails = GenericErrorDetails.newBuilder().withTimestamp(new Date().getTime())
				.withTitle(mrpException.getMessage()).withStatus(HttpStatus.UNAUTHORIZED.value())
				.withDetail("Token inv`alido").withDeveloperMessage(mrpException.getClass().getName()).build();

		return new ResponseEntity<>(mrpDetails, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleAnyException(Exception ex){
		GenericErrorDetails errorDetails = GenericErrorDetails.newBuilder().withTimestamp(new Date().getTime())
				.withTitle("Algo deu errado").withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.withDetail(ex.getClass().getName()).withDeveloperMessage(ex.getLocalizedMessage().intern()).build();
		
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
//	@ExceptionHandler(WebExchangeBindException.class)
//	public ResponseEntity<?> handleMissingRequiredPropsException(WebExchangeBindException mrpException){
//		String errors= mrpException.getAllErrors().get(1).;
//		
//		MissingRequiredPropsDetails errorDetails =  MissingRequiredPropsDetails.newBuilder().withTimestamp(new Date().getTime())
//				.withTitle("Alguns campos essenciais n~ao foram preenchidos").withStatus(HttpStatus.BAD_REQUEST.value())
//				.withDetail(mrpException.getClass().getName()).withDeveloperMessage(errors).build();
//		
//		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
//	}
}
