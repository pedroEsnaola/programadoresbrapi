package com.programadoresbr.model;

import org.springframework.security.core.GrantedAuthority;

public enum TipoCadastro implements GrantedAuthority {
	ADMIN("ADMIN"), USER("USER"), DEV("DEV");

	private final String nome;

	TipoCadastro(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public String getAuthority() {
		return nome;
	}

}
