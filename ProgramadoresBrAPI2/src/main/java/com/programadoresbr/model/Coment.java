package com.programadoresbr.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(indexes = {@Index(name="idx_coment_post", columnList = "post_id"),
		@Index(name="idx_coment_user", columnList = "user_id")})
public class Coment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(length = 600)
	private String body;

	@Nullable
	@Column(length = 10)
	private Long likes = (long) 0;

	@Nullable
	@Column(length = 10)
	private Long dislikes = (long) 0;

	@Nullable
	private String code;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_coment_user"))
	private User user;

	@OneToOne
	@JsonIgnoreProperties("coments")
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_coment_post"))
	private Post post;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Long getLikes() {
		return likes;
	}

	public void setLikes(Long likes) {
		this.likes = likes;
	}

	public Long getDislikes() {
		return dislikes;
	}

	public void setDislikes(Long dislikes) {
		this.dislikes = dislikes;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
