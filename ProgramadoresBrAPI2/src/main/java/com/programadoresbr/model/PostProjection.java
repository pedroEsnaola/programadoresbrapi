package com.programadoresbr.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "PostProjection", types = Post.class)
public interface PostProjection {

	@Value("#{target.id}")
	Long getId();

	@Value("#{target.body}")
	String getBody();

	@Value("#{target.title}")
	String getTitle();

}