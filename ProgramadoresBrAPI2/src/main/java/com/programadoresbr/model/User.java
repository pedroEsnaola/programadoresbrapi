package com.programadoresbr.model;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "users")
public class User implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	Long id;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<TipoCadastro> tipoCadastro;

	@NotNull
	@Column(name = "name", length = 60)
	private String name;

	@Column(name = "data_nascimento")
	private String dataNascimento;

	@Email
	@Column(name = "email", unique = true , length = 45)
	private String email;

	@NotNull
	@Column(name = "password")
	@JsonProperty(access = Access.WRITE_ONLY)
	private String senha;

	@Column(name = "data_cadastro", nullable = true)
	private Timestamp dataCadastro;

	private String profilePic = "/programadoresbr/user/default.png";

	@Pattern(regexp = "([0-9]{3}\\.?[0-9]{3}\\.?[0-9]{3}\\-?[0-9]{2})|([0-9]{2}\\.?[0-9]{3}\\.?[0-9]{3}\\.?\\/?[0-9]{4}\\-?[0-9]{2})")
	@Column(name = "cp", length = 19)
	private String cp;

	@Column(name = "status")
	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TipoCadastro> getTipoCadastro() {
		return tipoCadastro;
	}

	public void setTipoCadastro(List<TipoCadastro> tipoCadastro) {
		this.tipoCadastro = tipoCadastro;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataCadastro(Timestamp dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Timestamp getDataCadastro() {
		return dataCadastro;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return tipoCadastro;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public String getPassword() {
		// TODO Auto-generated method stub
		return senha;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return email;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)

	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
