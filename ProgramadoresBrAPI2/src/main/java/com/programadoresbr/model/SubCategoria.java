package com.programadoresbr.model;

public enum SubCategoria {
	BACKEND("Back-end"), FRONTEND("Front-end"), MOBILE("Mobile"), CLOUD("Cloud"), SQL("SQL"), NOSQL("NOSQL"),
	DESIGN("Design");

	private final String nome;

	SubCategoria(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
}
