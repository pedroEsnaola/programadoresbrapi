package com.programadoresbr.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(indexes = { @Index(name = "idx_pw_token_user", columnList = "user_id") })
public class PasswordToken {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	Date date;

	@OneToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_passwordToken_user"))
	User user;

	String token;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
