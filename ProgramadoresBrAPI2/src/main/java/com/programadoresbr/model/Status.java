package com.programadoresbr.model;

public enum Status {
	ATIVADO("ATIVADO"), DESATIVADO("DESATIVADO");

	private final String nome;

	Status(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
}
