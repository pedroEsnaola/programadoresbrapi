package com.programadoresbr.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.programadoresbr.model.TipoCadastro;
import com.programadoresbr.model.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private String secret = "programadoresbr";

	private String expirationTime = "259200";

	public Claims getAllClaimsFromToken(String token) {
		try {
			return Jwts.parser().setSigningKey(Base64.getEncoder().encodeToString(secret.getBytes("UTF-8"))).parseClaimsJws(token).getBody();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			
		}
		return null;
	}

	public String getUsernameFromToken(String token) {
		return getAllClaimsFromToken(token).getSubject();
	}

	public Date getExpirationDateFromToken(String token) {
		return getAllClaimsFromToken(token).getExpiration();
	}

	public String getPasswordFromToken(String token) {

		return getAllClaimsFromToken(token).get("password", String.class);
	}

	@SuppressWarnings("unchecked")
	public List<TipoCadastro> getRolesFromToken(String token) {
		Claims claims = getAllClaimsFromToken(token);
		List<String> rolesMap = claims.get("TipoCadastro", List.class);
		List<TipoCadastro> roles = new ArrayList<>();
		for (String rolemap : rolesMap) {
			roles.add(TipoCadastro.valueOf(rolemap));
		}
		return roles;

	}

	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	public String generateToken(User user) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("TipoCadastro", user.getAuthorities());
		claims.put("enable", true);
		claims.put("password", user.getPassword());
		return doGenerateToken(claims, user.getUsername());
	}

	private String doGenerateToken(Map<String, Object> claims, String username) {
		Long expirationTimeLong = Long.parseLong(expirationTime); // em segundos

		final Date createdDate = new Date();
		final Date expirationDate = new Date(createdDate.getTime() + expirationTimeLong * 1000);
		try {
		return Jwts.builder().setClaims(claims).setSubject(username).setIssuedAt(createdDate)
				.setExpiration(expirationDate).signWith(SignatureAlgorithm.HS256,Base64.getEncoder().encodeToString(secret.getBytes("UTF-8"))).compact();
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Boolean validateToken(String token) {
		return !isTokenExpired(token);
	}

}