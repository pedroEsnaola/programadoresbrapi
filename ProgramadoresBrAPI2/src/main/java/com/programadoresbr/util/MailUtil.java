package com.programadoresbr.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.programadoresbr.model.Message;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Component
public class MailUtil {

	@Autowired
	private JavaMailSender mailSender;

	public Scheduler mailScheduler() {
		return Schedulers.parallel();
	}

	public void send(Message message) {
		 
		
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom(message.getFrom());
			messageHelper.setTo(message.getTo());
			messageHelper.setSubject(message.getSubject());

			messageHelper.setText(message.getBody(), true);
		};
		
		
			
		try {
			mailSender.send(messagePreparator);
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		}	
		
		
}
