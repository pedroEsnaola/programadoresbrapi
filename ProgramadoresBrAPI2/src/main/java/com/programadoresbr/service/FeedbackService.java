package com.programadoresbr.service;

import com.programadoresbr.model.Feedback;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface FeedbackService {

	Flux<Feedback> getAllFeedbacks();

	Mono<Feedback> newFeedback(Feedback feedback, String userEmail);

	Mono<Feedback> deleteFeedbackById(Long id);

}
