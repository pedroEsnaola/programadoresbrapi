package com.programadoresbr.service;

import javax.mail.MessagingException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.codec.multipart.FilePart;

import com.programadoresbr.model.User;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

	Mono<User> saveUser(User user);

	Mono<Void> deleteById(Long id);

	Mono<User> findByEmail(String email);

	void deleteAll();

	boolean existsByEmail(String email);

	Mono<User> changeProfilePic(FilePart foto, String email);

	Mono<User> findById(Long id);

	Mono<Page<User>> findAll(Pageable pageable);

	Mono<Void> requestPasswordChange(String email);

	Mono<User> activateUser(Long id);

	Mono<User> changePassword(String newPassword, String token);
}
