package com.programadoresbr.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import javax.naming.directory.InvalidAttributesException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.programadoresbr.error.exception.MissingRequiredPropertyException;
import com.programadoresbr.model.Coment;
import com.programadoresbr.model.Post;
import com.programadoresbr.model.PostProjection;
import com.programadoresbr.repository.ComentRepository;
import com.programadoresbr.repository.PostRepository;
import com.programadoresbr.repository.UserRepository;
import com.programadoresbr.service.PostService;
import com.programadoresbr.util.FileUtil;

import javassist.NotFoundException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@Service
public class PostServiceImpl implements PostService {

	@Autowired
	Scheduler jdbcScheduler;

	@Autowired
	PostRepository postRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ComentRepository comentRepository;

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	FullTextEntityManager ftem;

	Long id;

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	FileUtil fileUtil;

	@Override
	public Mono<Post> newPost(Flux<FilePart> arquivo, String json, String email) {

		List<String> paths = new ArrayList<>();
		Callable<Post> retorno = new Callable<Post>() {

			@Override
			public Post call() throws Exception {
				Post post = null;
				try {
					post = mapper.readValue(json, Post.class);
				} catch (IOException e) {
					Mono.error(e);
				}
				post.setUser(userRepository.findByEmail(email));
				post = postRepository.save(post);

				id = post.getId();
				Mono<List<String>> path = arquivo.filter(part -> part instanceof FilePart).ofType(FilePart.class)
						.flatMap(this::save).collectList();
				try {
					path.toFuture().get().iterator().forEachRemaining(imagePath -> paths.add(imagePath));
				} catch (InterruptedException | ExecutionException e) {
					Mono.error(e);
				}
				post.setImagens(paths);
				postRepository.save(post);
				return post;
			}

			Mono<String> save(FilePart file) {
				return Mono.fromCallable(() -> fileUtil.salvarArquivo(file, "/forum_post/" + id, file.filename()));
			}
		};

		return Mono.fromCallable(retorno).subscribeOn(jdbcScheduler);

	}

	@Override
	public Flux<PostProjection> findPosts(String search, int page, int pageSize) {
		int firstResult = page*pageSize;
		QueryBuilder queryBuilder = ftem.getSearchFactory()
				.buildQueryBuilder()
				.forEntity(Post.class)
				.get();
		Query LuceneQuery = queryBuilder.phrase().withSlop(6).onField("body")
						.sentence(search.replace("-", " ")).createQuery();
		
		
		org.hibernate.search.jpa.FullTextQuery query = ftem.createFullTextQuery(LuceneQuery, Post.class).setMaxResults(pageSize).setFirstResult(firstResult);
		
		return Flux.fromIterable(query.getResultList());
		
	}

	@Override
	public Mono<Post> findPostById(Long postId) {
		
		return Mono.fromCallable(() -> {
			Optional<Post> post = postRepository.findById(postId);
			if (post.isEmpty()) Mono.error(new NotFoundException("Post não encotrado"));
				
			return post.get();
		}).subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<Post> updatePost(Post post, String email) {
		return Mono.fromCallable(()-> {
			if (post.getUser().getEmail().equals(email) && post.getId() != null) {
				return postRepository.save(post);
			} else if (post.getId() == null) {
				throw new MissingRequiredPropertyException("É preciso informar o ID para atualizar um post");
			} else {
				throw new InvalidAttributesException("As credenciais do Usuário não batem com o post");
			}
		});
	}

	@Override
	public Mono<Post> deletePost(Long idPost, String email) {
		return Mono.fromCallable(() -> {
			if (idPost == null) {
				throw new MissingRequiredPropertyException("É preciso informar o id do post para Exclui-lo");
			} else if (email.isEmpty() || email.equals(null)) {
				throw new MissingRequiredPropertyException("É preciso estar logado para excluir um post");
			}

			Optional<Post> post = postRepository.findById(idPost);
			if (post.get() == null) {
				throw new NotFoundException("Post não encotrado");
			} else if (post.get().getUser().getEmail().equals(email)) {

				postRepository.delete(post.get());
				return post.get();
			} else {
				throw new InvalidAttributesException("As credenciais do Usuário não batem com o post");
			}
		
		}).subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<Post> doComent(Long id, Coment coment, String email) {

		return Mono.fromCallable(() -> {
			Post post = postRepository.findById(id).get();
			coment.setPost(post);
			coment.setUser(userRepository.findByEmail(email));
			comentRepository.save(coment);

			return post;	
		}).subscribeOn(jdbcScheduler);
		}

	@Override
	@Transactional
	public void likeOneComent(Long id) {
		em.createNativeQuery("UPDATE coment SET likes = likes + 1  WHERE id =" + id);
	}

	@Override
	@Transactional
	public void dislikeOneComent(Long id) {
		em.createNativeQuery("UPDATE coment SET dislikes = dislikes + 1  WHERE id =" + id).executeUpdate();
	}

}
