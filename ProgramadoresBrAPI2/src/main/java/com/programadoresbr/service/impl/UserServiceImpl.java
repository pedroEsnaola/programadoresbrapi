package com.programadoresbr.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.Callable;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.mail.MailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.programadoresbr.error.exception.NotValidTokenException;
import com.programadoresbr.error.exception.UserAlreadyExistsException;
import com.programadoresbr.error.exception.UserNotFoundException;
import com.programadoresbr.model.Message;
import com.programadoresbr.model.PasswordToken;
import com.programadoresbr.model.Status;
import com.programadoresbr.model.User;
import com.programadoresbr.repository.PasswordTokenRepository;
import com.programadoresbr.repository.UserRepository;
import com.programadoresbr.service.UserService;
import com.programadoresbr.util.FileUtil;
import com.programadoresbr.util.MailUtil;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	MailUtil mailUtil;

	@Autowired
	UserRepository userRepository;

	@Autowired
	Scheduler jdbcScheduler;

	@Autowired
	MailSender mailSender;

	@Autowired
	PasswordTokenRepository tokenRepository;

	@Autowired
	private FileUtil fileUtil;

	@Override
	public Mono<User> saveUser(User user) {


				user.setDataCadastro(new Timestamp(new Date().getTime()));
				user.setSenha(passwordEncoder.encode(user.getPassword()));
				
				if (existsByEmail(user.getEmail()) == true) {
					throw new UserAlreadyExistsException(
							"Já existe um usuário com email " + user.getEmail() + " cadastrado");
				}

				user.setStatus(Status.DESATIVADO);
				User usuario = userRepository.save(user);
				try {
					enviaEmailValidacao(usuario);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return Mono.just(usuario);
			
	}

	@Override
	public Mono<Page<User>> findAll(Pageable pageable) {
		Mono<Page<User>> users = Mono.fromCallable(() -> {
			return userRepository.findAll(pageable);
		});

		return users.subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<User> findByEmail(String email) {
		return Mono.fromCallable(() -> {

			User user = userRepository.findByEmail(email);
			if (user == null) {
				throw new UserNotFoundException("Usuário " + email + " não encontrado");
			}
			return user;
		}).subscribeOn(jdbcScheduler);
	}

	@Override
	public void deleteAll() {
		userRepository.deleteAll();

	}

	@Override
	public Mono<Void> deleteById(Long id) {
		return Mono.fromCallable(() -> {

			if (userRepository.existsById(id) == false) {
				throw new UserNotFoundException("Usuário de id " + id + " não existe");
			}
			userRepository.deleteById(id);
			return (Void) null;
		}).subscribeOn(jdbcScheduler);
	}

	@Override
	public boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	void enviaEmailValidacao(User user) throws MessagingException {
		String subject = "Parabéns, para continuar, valide sua inscrição";
		Message message = new Message("ProgramadoresBR <contato@programadoresbr.com.br>", user.getEmail(), subject,
				"<!DOCTYPE html>\n" + "<html>\n" + "\n" + "    <!--Import Google Icon Font-->\n"
						+ "    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\n"
						+ "    <!--Import materialize.css-->\n"
						+ "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css\"/>\n"
						+ "    <!--Import JQuery-->\n"
						+ "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n"
						+ "\n" + "    <!--Let browser know website is optimized for mobile-->\n"
						+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n" + "\n"
						+ "\n" + "<body class=\"container\">\n" + "\n"
						+ "    <p>Para validar sua inscrição, clique aqui https://programadoresbr.com.br/"
						+ user.getId() + "/ativar" + "</p>\n" + "\n" + "</body>\n" + "</html>");
		new Thread(() -> {
			mailUtil.send(message);
		}).start();
	}

	@Override
	public Mono<User> changeProfilePic(FilePart foto, String email) {
		return Mono.fromCallable(() -> {

			User user = userRepository.findByEmail(email);
			String path = null;
			try {
				path = fileUtil.salvarArquivo(foto, "/user/profile_pic", user.getId() + "");
			} catch (IOException e) {
				e.printStackTrace();
			}
			user.setProfilePic(path);
			user = userRepository.save(user);
			return user;
		}).subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<User> findById(Long id) {
		return Mono.fromCallable(() -> userRepository.findById(id).get()).subscribeOn(jdbcScheduler);
	}

	public Mono<Long> count() {
		return Mono.fromCallable(() -> userRepository.count());
	}

	@Override
	public Mono<Void> requestPasswordChange(String email) {
		User user = userRepository.findByEmail(email);
		if (user == null) {
			throw new UserNotFoundException("Usuário " + email + " não encontrado");
		}

		PasswordToken passwordToken = new PasswordToken();

		String token = "";
		try {
			token = Base64.getEncoder().encodeToString(email.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		passwordToken.setToken(token);
		passwordToken.setUser(user);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 1);
		passwordToken.setDate(new java.sql.Date(c.getTimeInMillis()));
		tokenRepository.save(passwordToken);
		String subject = "Solicitação de mudança de Senha";
		Message message = new Message("ProgramadoresBR <contato@programadoresbr.com.br>", email, subject, "<!DOCTYPE html>\n"
				+ "<html>\n" + "\n" + "    <!--Import Google Icon Font-->\n"
				+ "    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\n"
				+ "    <!--Import materialize.css-->\n"
				+ "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css\"/>\n"
				+ "    <!--Import JQuery-->\n"
				+ "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n"
				+ "\n" + "    <!--Let browser know website is optimized for mobile-->\n"
				+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n" + "\n" + "\n"
				+ "<body class=\"container\">\n" + "\n"
				+ "    <p>Para altera sua senha, clique aqui https://programadoresbr.com.br/senha/alterar/" + token
				+ "</p>\n" + "\n" + "</body>\n" + "</html>");
		new Thread(() -> {
			mailUtil.send(message);
		}).start();
		return null;
	}

	@Override
	public Mono<User> changePassword(String newPassword, String token) {
		return Mono.fromCallable(() -> {

			PasswordToken passwordToken = tokenRepository.findByToken(token);
			if (passwordToken == null) {
				throw new UserNotFoundException("Token ou Usuario inexistentes");
			}
			if (passwordToken.getDate().before(new Date(Calendar.getInstance().getTimeInMillis()))) {
				throw new NotValidTokenException("O token para troca de Senha não e mais valido");
			}
			User user = passwordToken.getUser();
			user.setSenha(passwordEncoder.encode(newPassword));

			userRepository.save(user);
			tokenRepository.delete(passwordToken);
			return user;
		}).subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<User> activateUser(Long id) {
		return Mono.fromCallable(() -> {
			Optional<User> opUser = userRepository.findById(id);
			if (opUser.isEmpty())
				throw new UserNotFoundException("Usuario não encontrado");
			User user = opUser.get();
			user.setStatus(Status.ATIVADO);
			return userRepository.save(user);
		}).subscribeOn(jdbcScheduler);
	}

}
