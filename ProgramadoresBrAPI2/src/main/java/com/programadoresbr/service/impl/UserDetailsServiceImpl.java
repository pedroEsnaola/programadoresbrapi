package com.programadoresbr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.programadoresbr.model.User;
import com.programadoresbr.repository.UserRepository;

import reactor.core.publisher.Mono;

@Service
public class UserDetailsServiceImpl implements ReactiveUserDetailsService {

	@Autowired
	private UserRepository uRepository;

	@Override
	public Mono<UserDetails> findByUsername(String email) throws UsernameNotFoundException {
		User user = uRepository.findByEmail(email);
		if (user == null) {
			throw new UsernameNotFoundException("Usuario" + email + "Não encontrado");
		}

		return Mono.just(user);
	}

}
