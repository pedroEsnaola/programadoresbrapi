package com.programadoresbr.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.programadoresbr.model.Feedback;
import com.programadoresbr.repository.FeedBackRepository;
import com.programadoresbr.repository.UserRepository;
import com.programadoresbr.service.FeedbackService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@Service
public class FeedbackServiceImpl implements FeedbackService {

	@Autowired
	private FeedBackRepository feedbackRepository;

	@Autowired
	Scheduler jdbcScheduler;

	@Autowired
	private UserRepository userRepository;

	@Override
	public Flux<Feedback> getAllFeedbacks() {
		return Flux.fromIterable(feedbackRepository.findAll());
	}

	@Override
	public Mono<Feedback> newFeedback(Feedback feedback, String userEmail) {
		return Mono.just(feedback).map(f -> {
			f.setUser(userRepository.findByEmail(userEmail));

			return feedbackRepository.save(f);

		}).subscribeOn(jdbcScheduler);

	}

	@Override
	public Mono<Feedback> deleteFeedbackById(Long id) {
		return Mono.just(id).map(fId -> {
			Feedback feedback = feedbackRepository.findById(fId).get();
			feedback.setDeletedAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
			return feedbackRepository.save(feedback);
		}).subscribeOn(jdbcScheduler);
	}

}
