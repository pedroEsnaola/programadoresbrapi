package com.programadoresbr.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Card;
import com.julienvey.trello.domain.TList;
import com.julienvey.trello.impl.TrelloImpl;
import com.julienvey.trello.impl.http.ApacheHttpClient;

@Service
public class TrelloServiceImpl {

	private final Trello trelloApi = new TrelloImpl("ecf2952cf4e719a31d48d208b036e585", // aqui vai o key trello
			"707f272aa5264b5c311e2bacff78d80c369a3046144dbdbf2101317c7610ee08", // aqui vai o token trello
			new ApacheHttpClient());

	public List<TList> getBoards() {
		List<TList> boards = trelloApi.getBoard("w1AxObeN").fetchLists();// aqui deve ser posto o Board ID do trello)
		return boards;
	}

	public List<Card> getCards(@PathVariable String id) {
		List<Card> cards = trelloApi.getBoardCards("w1AxObeN");
		return cards;
	}

	public Card addCard(@RequestBody Card card) {
		Card card1 = trelloApi.createCard("5bcbe552befb693eb9429854", card);
		return card1;
	}

}
