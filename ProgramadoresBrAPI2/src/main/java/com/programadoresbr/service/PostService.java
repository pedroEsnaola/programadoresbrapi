package com.programadoresbr.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.codec.multipart.FilePart;

import com.programadoresbr.model.Coment;
import com.programadoresbr.model.Post;
import com.programadoresbr.model.PostProjection;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PostService {

	Mono<Post> newPost(Flux<FilePart> arquivo, String json, String email);

	Mono<Post> findPostById(Long id);

	Mono<Post> updatePost(Post post, String email);

	Mono<Post> deletePost(Long id, String email);

	Mono<Post> doComent(Long id, Coment coment, String email);

	void likeOneComent(Long id);

	void dislikeOneComent(Long id);

	Flux<PostProjection> findPosts(String search, int page, int pageSize);

}
